## iOS：OpenGLES 实验室之2D篇 第三弹 の 直播白板

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;笔者之前发表的音视频文章，有图像的处理，音频的重采样等等，都属于入门级别。通过阅读它们，读者能对音视频有了了解。可在 [Gitee](https://gitee.com/chenqihui) 上面回顾。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2022 年，笔者将整理下 关于 OpenGLES 的实验室系列 并进行发表。首先为读者带来2D篇的系列，它大多是 x y 坐标，不涉及 z 坐标，所以用 2D篇。内容上，它不对 OpenGLES 的基础知识进行细说与讨论。但如果对 OpenGLES 不了解或者了解一点，仍可通过本实验室系列了解 OpenGLES。它旨在激起读者的兴趣，扩展到实际的应用上。总的来说，这些实验 & Demo 将是额外的，即对基础学习的补充，通过这些它们的实践和运用，能让读者进一步了解 OpenGLES 。

### 前言

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本次实验室带来的是《OpenGLES 实验室之2D篇 第三弹 の 直播白板》。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;直播白板应用在搜狐视频App直播，配合播主的教学，在白板上展示英语、物理、历史等图片或者手绘文字。更加生动且有趣的进行讲解，并且还可以在助手App使用 PPT 等准备好的教材，让直播内容更加便捷，且素材丰富。助手还提供了大白板和小白板的切换来展示不同白板的效果，更充分和自由的展示白板，也让观众可以学习更多的知识。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;而直播白板的原理是将另一部设备（助手端）的截图数据传到直播的手机上，然后将该图片进行主播端展示和推流的混流。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;而本 Demo 只包含采集，不包含推流，所以摄像头采集的数据，与白板图片（固定图片）进行混流后回显。其实该混流输出的这个 output 就是要推流给观众观看的直播流画面。可能有读者会问，那为什么直播预览不直接用混流回显？其实是可以的，但缺少灵活性。主播端分开展示，可以比较好的满足主播端的多样性，如实现大小白板（不同白板大小）切换的动画，主播预览的自定义区域设置等等。

### Demo

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demo 分别是直播实现的 多重纹理（顶部小白板） 和 多通道渲染（全屏大白板）。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[QHWhiteBoardMan: iOS：OpenGLES 实验室之2D篇 第三弹 の 直播白板](https://gitee.com/chenqihui/qhwhite-board-man)

### 实现

#### 效果

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;搜狐视频App的白板效果如下：

<table>
    <tr>
        <td ><center><img width = "281" height = "609" src="./imgs/3.PNG">大白板</center></td>
        <td ><center><img width = "281" height = "609" src="./imgs/4.PNG">小白板</center></td>
    </tr>
</table>

#### 结构图

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;多通道渲染 采用多纹理多次绘制，而多重纹理 采用多纹理单次绘制。

![](./imgs/Diagram.png)

#### 多通道渲染

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通过 管道组合 进行一层一层绘制，跟画画很像。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**多通道渲染的顺序**很重要，由于大白板的主播画面是白板的上面，所以需要先绘制白板，再绘制主播画面，否则白板会覆盖在主播画面，这样主播就预览不到自己。

~~~C
// 第一层：白板
glViewport(0, 0, (int)_frameWidth, (int)_frameHeight);
        
glActiveTexture(GL_TEXTURE1);
glBindTexture(GL_TEXTURE_2D, _texId1);
glUniform1i(_filterInputTextureUniform, 1);

glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

// 第二层：主播画面
CGFloat d = 4;
glViewport(0, 0, (int)_frameWidth/d, (int)_frameWidth/d);

glActiveTexture(GL_TEXTURE2);
glBindTexture(GL_TEXTURE_2D, _texId2);
glUniform1i(_filterInputTextureUniform2, 2);

glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

// 最终
glViewport(0, 0, (int)_frameWidth, (int)_frameHeight);
~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shader 只需直接输出像素

~~~ObjC
NSString *const rgbFragmentShaderString = SHADER_STRING
(
 varying highp vec2 v_texcoord;
 uniform sampler2D inputImageTexture;
 
 void main()
 {
     gl_FragColor = vec4(texture2D(inputImageTexture, v_texcoord).bgr, 1);
 }
);
~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;效果如下：

<div align="center">  
<img src="./imgs/1.PNG" width = "281" height = "609" alt="图片名称" align=center/>
</div>

#### 多重纹理

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;可见与多通道渲染明显的不同就是少了一次 glDrawArrays，它不仅仅是一次绘制，其包含的执行可看下一章节：**对比**

~~~C
glViewport(0, 0, (int)_frameWidth, (int)_frameHeight);
        
glActiveTexture(GL_TEXTURE1);
glBindTexture(GL_TEXTURE_2D, _texId1);
glUniform1i(_filterInputTextureUniform, 1);

glActiveTexture(GL_TEXTURE2);
glBindTexture(GL_TEXTURE_2D, _texId2);
glUniform1i(_filterInputTextureUniform2, 2);

glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shader 需要增加多一个 sampler2D 图数据变量，并进行区域控制，或者进行 mix。

~~~ObjC
NSString *const rgbFragmentShaderString2 = SHADER_STRING
(
 varying highp vec2 v_texcoord;
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 void main()
 {
    if (v_texcoord.y < 0.5) {
        gl_FragColor = vec4(texture2D(inputImageTexture2, v_texcoord).bgr, 1);
    }
    else {
        gl_FragColor = vec4(texture2D(inputImageTexture, v_texcoord).bgr, 1);
    }
 }
);
~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;效果如下：

<div align="center">  
<img src="./imgs/2.PNG" width = "281" height = "609" alt="图片名称" align=center/>
</div>

#### 对比

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;多通道渲染 与 多重纹理 的对比

1、多通道渲染会多次绘制**glDrawArrays**，而多重纹理则绘制一次；

2、第二次后的绘制都要进行 前像素颜色的读取，并和当前像素颜色混合（如果允许 alpha，则它们会进行 mix，《iOS：OpenGLES 实验室之2D篇 第一弹 の 智能弹幕》介绍过），这些内部操作，即需要的更多性能开销，只要执行一次就多一次内部开销；

3、多通道渲染可通过**glViewport**更改每次绘制的视图区域，而多重纹理则不行，因为视图只有一个，但可以在 fsh 上处理，相对比较麻烦，还得进行图片的自行缩放；

4、多通道渲染的管道组合下，可动态添加图层。多重纹理需要在 fsh 里提前添加 纹理数据变量，并进行读取才可以。Apple 的 GLKBaseEffect 支持 2 个 GLKVertexAttribTexCoord。

#### 纹理缓存

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;首先将 白板图片的数据保存到 缓存，使用《iOS：OpenGLES 实验室之2D篇 第二弹 の 瘦脸修图》的一样的实现，直接将 png 转为 rgba 的原始数据，上传到共享缓存，并绑定 纹理id。

~~~C
glGenTextures(1, &_texId2);
glBindTexture(GL_TEXTURE_2D, _texId2);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
unsigned char *pBGRAImageIn;
[QHUtil input:@"WhiteBoard_rgba" ofType:@"rgb" len:&pBGRAImageIn];
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)1125, (GLsizei)2436,
             0, GL_RGBA, GL_UNSIGNED_BYTE, pBGRAImageIn);
glBindTexture(GL_TEXTURE_2D, 0);
~~~

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;激活纹理后使用，因为前面已经将图片缓存到共享内容，GPU 可以通过 textureId 直接获取数据。

~~~C
glActiveTexture(GL_TEXTURE2);
glBindTexture(GL_TEXTURE_2D, _texId2);
glUniform1i(_filterInputTextureUniform2, 2);
~~~

### Android Demo

#### Demo上 Android 与 iOS 的相同点

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在 OpenGLES，在调用上和转化纹理有稍微不同，当主流程是一样， 都是 多通道渲染 先绘制白板，再绘制摄像头采集，而多重纹理则同时上传两个纹理进行位置的判断来选择渲染。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;而 GLSL 的编写则都是一样的。

#### 说明

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;多通道渲染采用 Java 层调用 GLES，多重纹理 则采用 CPP 层调用 GLES，这应该是 Android 端调用 OpenGLES 的两种方式。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;然后白板也使用两种上传纹理的方式，一种是 bitmap，一种是 RGB Data。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;由于笔者编写 Android 能力有限，所以 Demo 的 多通道渲染是参考 **安卓同事（曾哥童鞋）** 的声网自采集水印，多重纹理是参考 **《音视频开发进阶指南——展晓凯/魏晓红》** 的采集例子。因此 Demo 里面对于 Camera 和 GLES 的封装和调用，笔者基本原封保留，这边对以上参考表示感谢。

#### 效果

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;由于白板的比例不一致，为保持 GLSL 写法一致，所以没处理变形，请忽略。

<center><table>
    <tr>
        <td><center><img width = "216" height = "560" src="./imgs/5.jpg"/></center><center>多通道渲染</center></td>
        <td><center><img width = "216" height = "560" src="./imgs/6.jpg"/></center><center>多重纹理</center></td>
    </tr>
</table></center>


### 最后

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;实际中，直播白板的内容是改变的，会不停传图片数据，然后进行更新。这其实才是 OpenGLES 混流最大的性能瓶颈里，因为大量的改变会进行很大的内存交换。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总的来说，如何选择使用 多通道渲染 还是 多重纹理，它们都能做到相同的混流效果，只是编写的实现不一样，还需根据实际需求进行选择应用哪种实现技术。而对直播而言，会使用超过两个图层，如直播水印，直播昵称，水印广告，大小白板，logo 等等，所以倾向于 多通道渲染，它在直播推流器的框架设计更具扩展性。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;感谢各位读者，本实验室之2D篇，完结啦👋！

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;欢迎读者回顾本系列的文章：

* [《iOS：OpenGLES 实验室之2D篇 第一弹 の 智能弹幕》](https://gitee.com/chenqihui/qhaidanmu-man)
* [《iOS：OpenGLES 实验室之2D篇 第二弹 の 瘦脸修图》](https://gitee.com/chenqihui/qhdraw-bitmap-mesh-man)
* [《iOS：OpenGLES 实验室之2D篇 第三弹 の 直播白板》](https://gitee.com/chenqihui/qhwhite-board-man)

### 链接

* 《Gitee》—— https://gitee.com/chenqihui
* 《QHWhiteBoardMan: iOS：OpenGLES 实验室之2D篇 第三弹 の 直播白板》—— https://gitee.com/chenqihui/qhwhite-board-man
* 《BradLarson/GPUImage: An open source iOS framework for GPU-based image and video processing》—— https://github.com/BradLarson/GPUImage