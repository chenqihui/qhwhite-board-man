//
//  QHUtil.h
//  QH_Simplest_Mediadata_Demo
//
//  Created by Anakin chen on 2021/2/7.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QHUtil : NSObject

+ (NSUInteger)input:(NSString *)name ofType:(nullable NSString *)ext len:(unsigned char *_Nullable*_Nullable)output;
+ (FILE *)output:(NSString *)file;
+ (uint)hexToDecimal:(unsigned char *)buf len:(uint)length;
+ (FILE *)copy2Caches:(NSString *)name ofType:(nullable NSString *)ext;
+ (void)fullScreen:(UIView *)subView;

@end

NS_ASSUME_NONNULL_END
