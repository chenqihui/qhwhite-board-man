//
//  simplest_eagl_rgb_render_view.m
//  QH_Simplest_EAGL_Demo
//
//  Created by Anakin chen on 2021/2/17.
//

#import "simplest_eagl_rgb_render_view.h"

@implementation simplest_eagl_rgb_render_view

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[self layer];
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
    }
    return self;
}

@end
