//
//  simplest_eagl.h
//  QH_Simplest_EAGL_Demo
//
//  Created by Anakin chen on 2021/2/17.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <CoreVideo/CoreVideo.h>
#import <AVFoundation/AVFoundation.h>

#import "QHUtil.h"

NS_ASSUME_NONNULL_BEGIN

@interface simplest_eagl : NSObject

@property (nonatomic, strong) EAGLContext *eaglContext;

@property (nonatomic) dispatch_queue_t videoQueue;
@property (nonatomic) dispatch_semaphore_t frameRenderingSemaphore;

@property (nonatomic, weak) UIView *preview;

+ (instancetype)createWith:(UIView *)preview;
- (void)captureOutput;

- (void)p_setup;
- (void)p_processVideoSampleBuffer;

@end

NS_ASSUME_NONNULL_END
