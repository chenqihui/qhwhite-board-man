//
//  simplest_eagl_rgb_render_shader.h
//  QH_Simplest_EAGL_Demo
//
//  Created by Anakin chen on 2021/2/17.
//

#ifndef simplest_eagl_rgb_render_shader_h
#define simplest_eagl_rgb_render_shader_h

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define SHADER_STRING(text) @ STRINGIZE2(text)

NSString *const vvertexShaderString = SHADER_STRING
(
 attribute vec4 position;
 attribute vec2 texcoord;
 varying vec2 v_texcoord;
 
 void main()
 {
     gl_Position = position;
     v_texcoord = texcoord.xy;
 }
);

NSString *const rgbFragmentShaderString = SHADER_STRING
(
 varying highp vec2 v_texcoord;
 uniform sampler2D inputImageTexture;
 
 void main()
 {
     gl_FragColor = vec4(texture2D(inputImageTexture, v_texcoord).bgr, 1);
 }
);

NSString *const rgbFragmentShaderString2 = SHADER_STRING
(
 varying highp vec2 v_texcoord;
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 void main()
 {
    if (v_texcoord.y < 0.5) {
        gl_FragColor = vec4(texture2D(inputImageTexture2, v_texcoord).bgr, 1);
    }
    else {
        gl_FragColor = vec4(texture2D(inputImageTexture, v_texcoord).bgr, 1);
    }
 }
);

#endif /* simplest_eagl_rgb_render_shader_h */
