//
//  simplest_eagl_rgb_render.h
//  QH_Simplest_EAGL_Demo
//
//  Created by Anakin chen on 2021/2/17.
//

#import "simplest_eagl.h"

NS_ASSUME_NONNULL_BEGIN

@interface simplest_eagl_rgb_render : simplest_eagl

+ (instancetype)createWith:(UIView *)preview type:(int)type;
- (void)processSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)addWbImage;

@end

NS_ASSUME_NONNULL_END
