//
//  QHMediaLog.m
//  QHMediaLiveDemo
//
//  Created by qihuichen on 2022/7/28.
//

#import "QHMediaLog.h"

@implementation QHMediaLog

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static QHMediaLog *this;
    dispatch_once(&onceToken, ^{
        this = [QHMediaLog new];
    });
    return this;
}

- (void)log:(QHMediaLogLevel)level content:(NSString *)c {
    NSLog(@"%@", c);
}

+ (void)log:(QHMediaLogLevel)level content:(NSString *)c {
    NSLog(@"%@", c);
}

@end
