//
//  QHMediaLog.h
//  QHMediaLiveDemo
//
//  Created by qihuichen on 2022/7/28.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    QHMediaLogLevelNormal,
    QHMediaLogLevelDebug,
    QHMediaLogLevelError,
} QHMediaLogLevel;

NS_ASSUME_NONNULL_BEGIN

@interface QHMediaLog : NSObject

- (void)log:(QHMediaLogLevel)level content:(NSString *)c;
+ (void)log:(QHMediaLogLevel)level content:(NSString *)c;

@end

NS_ASSUME_NONNULL_END
