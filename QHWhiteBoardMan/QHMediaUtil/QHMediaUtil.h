//
//  QHMediaUtil.h
//  QHMediaLiveDemo
//
//  Created by qihuichen on 2022/6/26.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define weakObj(o) __weak typeof(o) o##Weak = o;
#define strongObj(o) __strong typeof(o) o##Strong = o;

@interface QHMediaUtil : NSObject

+ (NSURL *)watermark:(CGSize)s;

@end

NS_ASSUME_NONNULL_END
