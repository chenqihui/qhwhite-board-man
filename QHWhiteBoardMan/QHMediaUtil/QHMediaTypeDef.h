//
//  QHMediaTypeDef.h
//  QHMediaLiveDemo
//
//  Created by qihuichen on 2022/6/26.
//

#ifndef QHMediaTypeDef_h
#define QHMediaTypeDef_h

/// 直播场景 (QHMedia内部会根据场景的特征进行参数调优)
typedef NS_ENUM(NSUInteger, QHMediaLiveScene) {
    /// 默认通用场景(不确定场景时使用)
    QHMediaLiveScene_Default = 0,
    /// 秀场场景, 主播上半身为主
    QHMediaLiveScene_Showself,
    /// 游戏场景
    QHMediaLiveScene_Game,
    // others comming soon
};

/// 录制场景
typedef NS_ENUM(NSUInteger, QHMediaRecScene) {
    /// 恒定码率场景
    QHMediaRecScene_ConstantBitRate = 0,
    /// 恒定质量场景
    QHMediaRecScene_ConstantQuality,
};

/// 视频编码性能档次 (视频质量 和 设备资源之间的权衡)
typedef NS_ENUM(NSUInteger, QHMediaVideoEncodePerformance) {
    /// 低功耗:  cpu资源消耗低一些,视频质量差一些
    QHMediaVideoEncodePer_LowPower= 0,
    /// 均衡档次: 性价比比较高
    QHMediaVideoEncodePer_Balance,
    /// 高性能: 画面质量高
    QHMediaVideoEncodePer_HighPerformance,
};

#pragma mark - Authorization

/// 设备授权状态
typedef NS_ENUM(NSUInteger, QHMediaDevAuthStatus) {
    /// 还没有确定是否授权
    QHMediaDevAuthStatusNotDetermined = 0,
    /// 设备受限，一般在家长模式下设备会受限
    QHMediaDevAuthStatusRestricted,
    /// 拒绝授权
    QHMediaDevAuthStatusDenied,
    /// 已授权
    QHMediaDevAuthStatusAuthorized
};

#pragma mark - Video Dimension

/// 采集分辨率
typedef NS_ENUM(NSUInteger, QHMediaVideoDimension) {
    /// 16 : 9 宽高比，1280 x 720 分辨率
    QHMediaVideoDimension_16_9__1280x720 = 0,
    /// 16 : 9 宽高比，960 x 540 分辨率
    QHMediaVideoDimension_16_9__960x540,
    /// 4 : 3 宽高比，640 x 480 分辨率
    QHMediaVideoDimension_4_3__640x480,
    /// 16 : 9 宽高比，640 x 360 分辨率
    QHMediaVideoDimension_16_9__640x360,
    /// 4 : 3 宽高比，320 x 240 分辨率
    QHMediaVideoDimension_5_4__352x288,
    
    /// 缩放自定义分辨率 从设备支持的最近分辨率缩放获得, 若设备没有对应宽高比的分辨率，则裁剪后进行缩放
    QHMediaVideoDimension_UserDefine_Scale,
    /// 裁剪自定义分辨率 从设备支持的最近分辨率裁剪获得
    QHMediaVideoDimension_UserDefine_Crop,
    /// 注意： 选择缩放自定义分辨率时可能会有额外CPU代价
    
    /// 默认分辨率，默认为 4 : 3 宽高比，640 x 480 分辨率
    QHMediaVideoDimension_Default = QHMediaVideoDimension_4_3__640x480,
};

#pragma mark - Video & Audio Codec ID
/*!
 * @abstract  视频编码器类型
 */
typedef NS_ENUM(NSUInteger, QHMediaVideoCodec) {
    /// 视频编码器 - h264 软件编码器
    QHMediaVideoCodec_X264 = 0,
    /// 视频编码器 - QHMedia265 软件编码器
    QHMediaVideoCodec_QY265,
    /// 视频编码器 - iOS VT264硬件编码器 (iOS 8.0以上支持)
    QHMediaVideoCodec_VT264,
    /// 视频编码器 - iOS VT265硬件编码器 (iOS 11.0 以上)
    QHMediaVideoCodec_VT265,
    /// 视频编码器 - 由SDK自动选择（ VT264 > X264）
    QHMediaVideoCodec_AUTO = 100,
    /// 视频编码器 - gif
    QHMediaVideoCodec_GIF,
};

/*!
 * @abstract  音频编码器类型
 */
typedef NS_ENUM(NSUInteger, QHMediaAudioCodec) {
    /// aac音频软件编码器 - AAC_HE
    QHMediaAudioCodec_AAC_HE = 0,
    /// aac音频软件编码器 - AAC_LC
    QHMediaAudioCodec_AAC,
    /// iOS自带的audiotoolbox音频编码器 - AAC_LC (CPU占用较低,但推荐码率:64kbps单声道,128kbps双声道)
    QHMediaAudioCodec_AT_AAC,
    /// aac软件编码器 - AAC_HE_V2 (仅双声道音频可用;如果输入数据为单声道,则自动退化为 AAC_HE)
    QHMediaAudioCodec_AAC_HE_V2,
};

#pragma mark - QYPublisher State

/*!
 * @abstract  采集设备状态
 */
typedef NS_ENUM(NSUInteger, QHMediaCaptureState) {
    /// 设备空闲中
    QHMediaCaptureStateIdle,
    /// 设备工作中
    QHMediaCaptureStateCapturing,
    /// 设备授权被拒绝
    QHMediaCaptureStateDevAuthDenied,
    /// 关闭采集设备中
    QHMediaCaptureStateClosingCapture,
    /// 参数错误，无法打开（比如设置的分辨率，码率当前设备不支持）
    QHMediaCaptureStateParameterError,
    /// 设备正忙，请稍后尝试 ( 该状态在发出通知0.5秒后被清除 ）
    QHMediaCaptureStateDevBusy,
};

/*!
 * @abstract  推流状态
 */
typedef NS_ENUM(NSUInteger, QHMediaStreamState) {
    /// 初始化时状态为空闲
    QHMediaStreamStateIdle = 0,
    /// 连接中
    QHMediaStreamStateConnecting,
    /// 已连接
    QHMediaStreamStateConnected,
    /// 断开连接中
    QHMediaStreamStateDisconnecting,
    /// 推流出错
    QHMediaStreamStateError,
};

/*!
 * @abstract  推流错误码，用于指示推流失败的原因
 */
typedef NS_ENUM(NSUInteger, QHMediaStreamErrorCode) {
    /// 正常无错误
    QHMediaStreamErrorCode_NONE = 0,
    /// (obsolete)
    QHMediaStreamErrorCode_QHMediaAUTHFAILED __deprecated_enum_msg("auth removed"),
    /// 当前帧编码失败
    QHMediaStreamErrorCode_ENCODE_FRAMES_FAILED,
    /// 无法打开配置指示的CODEC
    QHMediaStreamErrorCode_CODEC_OPEN_FAILED,
    /// 连接出错，检查地址
    QHMediaStreamErrorCode_CONNECT_FAILED,
    /// 网络连接中断
    QHMediaStreamErrorCode_CONNECT_BREAK,
    /// rtmp 推流域名不存在 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_NonExistDomain,
    /// rtmp 应用名不存在(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_NonExistApplication,
    /// rtmp 流名已存在(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_AlreadyExistStreamName,
    /// rtmp 被黑名单拒绝(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_ForbiddenByBlacklist,
    /// rtmp 内部错误(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_InternalError,
    /// rtmp URL 地址已过期(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_URLExpired,
    /// rtmp URL 地址签名错误(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_SignatureDoesNotMatch,
    /// rtmp URL 中AccessKeyId非法(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_InvalidAccessKeyId,
    /// rtmp URL 中参数错误(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_BadParams,
    /// rtmp URL 中的推流不在发布点内（QHMedia 自定义）
    QHMediaStreamErrorCode_RTMP_ForbiddenByRegion,
    /// (obsolete)
    QHMediaStreamErrorCode_FRAMES_THRESHOLD,
    /// (obsolete)
    QHMediaStreamErrorCode_NO_INPUT_SAMPLE,
    /// 对于URL中的域名解析失败
    QHMediaStreamErrorCode_DNS_Parse_failed,
    /// 对于URL对应的服务器连接失败(无法建立TCP连接)
    QHMediaStreamErrorCode_Connect_Server_failed,
    /// 跟RTMP服务器完成握手后,向{appname}/{streamname} 推流失败
    QHMediaStreamErrorCode_RTMP_Publish_failed,
    /// 音视频同步失败 (输入的音频和视频的时间戳的差值超过5s)
    QHMediaStreamErrorCode_AV_SYNC_ERROR,
    /// 非法地址(地址为空或url中的协议或本地文件的后缀SDK不支持, 请检查)
    QHMediaStreamErrorCode_INVALID_ADDRESS,
    /// 网络不通
    QHMediaStreamErrorCode_NETWORK_UNREACHABLE,
    /// 获取user id失败 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_GetUserIdFailed,
    /// AK和user id不匹配 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_AkAndUserIsNotMatch,
    /// 获取服务器信息失败 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_GetServerInfoFailed,
    /// 非法的外部url (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_IllegalOutsideUrl,
    /// 外部鉴权失败 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_OutsideAuthFailed,
    /// 简单鉴权失败(QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_SimpleAuthFailed,
    /// 无效的鉴权类型 (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_InvalidAuthType,
    /// 非法的user id (QHMedia 自定义)
    QHMediaStreamErrorCode_RTMP_IllegalUserId,
};

/*!
 * @abstract  网络状况事件码，用于指示当前网络健康状况
 */
typedef NS_ENUM(NSUInteger, QHMediaNetStateCode) {
    /// 正常无错误
    QHMediaNetStateCode_NONE = 0,
    /// 发送包时间过长，( 单次发送超过 500毫秒 ）
    QHMediaNetStateCode_SEND_PACKET_SLOW,
    /// 估计带宽调整，上调
    QHMediaNetStateCode_EST_BW_RAISE,
    /// 估计带宽调整，下调
    QHMediaNetStateCode_EST_BW_DROP,
    /// 帧率调整，上调
    QHMediaNetStateCode_VIDEO_FPS_RAISE,
    /// 帧率调整，下调
    QHMediaNetStateCode_VIDEO_FPS_DROP,
    /// SDK 鉴权失败 (暂时正常推流5~8分钟后终止推流)
    QHMediaNetStateCode_QHMediaAUTHFAILED,
    /// 输入音频不连续
    QHMediaNetStateCode_IN_AUDIO_DISCONTINUOUS,
    /// 网络变为不可用
    QHMediaNetStateCode_UNREACHABLE,
    /// 网络变为可用
    QHMediaNetStateCode_REACHABLE,
};

/*!
 * @abstract  音频播放状态
 */
typedef NS_ENUM(NSUInteger, QHMediaBgmPlayerState) {
    /// 初始状态
    QHMediaBgmPlayerStateInit,
    /// 背景音正在播放
    QHMediaBgmPlayerStateStarting,
    /// 背景音停止
    QHMediaBgmPlayerStateStopped,
    /// 背景音正在播放
    QHMediaBgmPlayerStatePlaying,
    /// 背景音暂停
    QHMediaBgmPlayerStatePaused,
    /// 背景音播放出错
    QHMediaBgmPlayerStateError,
    /// 背景音被打断
    QHMediaBgmPlayerStateInterrupted
};


/*!
 * @abstract  音频输入设备类型
 * @discussion 参考 AVAudioSessionPortBuiltInMic
 */
typedef NS_ENUM(NSUInteger, QHMediaMicType) {
    /// Built-in microphone on an iOS device
    QHMediaMicType_builtinMic = 0,
    /// Microphone on a wired headset
    QHMediaMicType_headsetMic,
    /// 蓝牙设备
    QHMediaMicType_bluetoothMic,
    
    /// 未知设备
    QHMediaMicType_unknow = 1000,
};

 /*!
  * @abstract  网络自适应模式类型
  */
typedef NS_ENUM(NSUInteger, QHMediaBWEstimateMode) {
    /// 默认模式 (综合模式,比较平稳)
    QHMediaBWEstMode_Default = 0,
    /// 流畅优先模式(消极上调, 极速下调)
    QHMediaBWEstMode_Negtive,
    
    /// 禁用网络自适应网络调整
    QHMediaBWEstMode_Disable = 1000,
};

/*!
 * @abstract  旁路录制状态
 */
typedef NS_ENUM(NSInteger, QHMediaRecordState) {
    /// 初始状态
    QHMediaRecordStateIdle,
    /// 录像中
    QHMediaRecordStateRecording,
    /// 录像停止
    QHMediaRecordStateStopped,
    /// 录像失败
    QHMediaRecordStateError,
};

/*!
 * @abstract  旁路录制错误码
 */
typedef NS_ENUM(NSInteger, QHMediaRecordError) {
    /// 无错误
    QHMediaRecordErrorNone,
    /// 地址错误
    QHMediaRecordErrorPathInvalid,
    /// 格式不支持
    QHMediaRecordErrorFormatNotSupport,
    /// 内部错误
    QHMediaRecordErrorInternal,
};


/*!
 * 推流的Qos信息
 */
typedef struct QHMediaStreamerQosInfo {
    /// 当前缓冲区中待发送的音频数据大小, 单位是Bytes
    int audioBufferDataSize;
    /// 当前缓冲区中待发送的音频数据时间长度, 单位是ms
    int audioBufferTimeLength;
    /// 当前缓冲区中待发送的音频数据包个数
    int audioBufferPackets;
    /// 从开始推流到当前时间点已编码的音频数据大小, 单位是Bytes
    int64_t audioTotalDataSize;

    /// 当前缓冲区中待发送的视频数据大小, 单位是Bytes
    int videoBufferDataSize;
    /// 当前缓冲区中待发送的视频数据时间长度, 单位是ms
    int videoBufferTimeLength;
    /// 当前缓冲区中待发送的视频数据包个数
    int videoBufferPackets;
    /// 从开始推流到当前时间点已编码的音频数据大小, 单位是Bytes
    int64_t videoTotalDataSize;
}QHMediaStreamerQosInfo;

/*!
 * @abstract  网络链接状态
 */
typedef NS_ENUM(NSInteger, QHMediaNetReachState) {
    /// 未知
    QHMediaNetReachState_Unknown,
    /// 联网
    QHMediaNetReachState_OK,
    /// 断网
    QHMediaNetReachState_Bad,
};

#endif /* QHMediaTypeDef_h */
