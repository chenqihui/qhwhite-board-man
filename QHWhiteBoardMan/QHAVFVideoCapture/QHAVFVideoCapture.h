//
//  QHAVFVideoCapture.h
//  QHWhiteBoardMan
//
//  Created by qihuichen on 2022/10/31.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QHAVFVideoCapture : NSObject

@property (nonatomic, strong, readonly) AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic, readonly, getter=isRunnig) BOOL running;

@property (nonatomic, readwrite) AVCaptureSessionPreset vPreset;
@property (nonatomic) int32_t frameRate;
@property (nonatomic) OSType outputPixelFmt;

@property (nonatomic, copy) void(^vProcessingCallback)(CMSampleBufferRef sampleBuffer);
@property (nonatomic, copy) void(^vInterruptCallback)(BOOL bInterrupt);

- (instancetype)initWith:(NSString *)vPreset cameraPosition:(AVCaptureDevicePosition)vPosition;

- (void)start;
- (void)stop;
- (void)pause;
- (void)resume;
- (void)rotateCamera;
- (void)toggleTorch;

@end

NS_ASSUME_NONNULL_END
