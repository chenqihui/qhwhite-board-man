package com.chen.qhwhiteboardman.capture.camera;

import android.content.Context;

import com.chen.qhwhiteboardman.capture.framework.modules.channels.ChannelManager;
import com.chen.qhwhiteboardman.capture.framework.modules.channels.VideoChannel;

public class CameraVideoChannel extends VideoChannel {
    private static final String TAG = CameraVideoChannel.class.getSimpleName();

    private VideoCapture mVideoCapture;
    private volatile boolean mCapturedStarted;

    private int mWidth = 1280;
    private int mHeight = 720;
    private int mFrameRate = 24;
    private int mFacing = Constant.CAMERA_FACING_FRONT;

    public CameraVideoChannel(Context context, int id) {
        super(context, id);
    }

    @Override
    protected void onChannelContextCreated() {
        mVideoCapture = VideoCaptureFactory.createVideoCapture(getChannelContext().getContext());
    }

    /**
     * Set the current camera facing
     * @param facing must be one of Constant.CAMERA_FACING_FRONT
     *               or Constant.CAMERA_FACING_BACK
     * Will not take effect until next startCapture or
     * switchCamera succeeds.
     */
    public void setFacing(int facing) {
        mFacing = facing;
    }

    public int getFacing() {
        return mFacing;
    }

    /**
     * Set the ideal capture image size in pixels.
     * Note the size is only a reference to find the
     * most closest size that the camera hardware supports.
     * The size is usually horizontal, that is, the width
     * is larger than the height, or the picture will be
     * cropped more than desired.
     * The default picture size is 1920 * 1080
     * Will not take effect until next startCapture or
     * switchCamera succeeds.
     */
    public void setPictureSize(int width, int height) {
        mWidth = width;
        mHeight = height;
        setPreProcessorSize(width,height);
    }

    public void setIdealFrameRate(int frameRate) {
        mFrameRate = frameRate;
    }

    public void startCapture() {
        if (isRunning()) {
            getHandler().post(() -> {
                if (!mCapturedStarted) {
                    mVideoCapture.connectChannel(ChannelManager.ChannelID.CAMERA);
                    mVideoCapture.setSharedContext(getChannelContext().getEglCore().getEGLContext());
                    mVideoCapture.allocate(mWidth, mHeight, mFrameRate, mFacing);
                    mVideoCapture.startCaptureMaybeAsync(false);
                    mCapturedStarted = true;
                }
            });
        }
    }


    public void resetCapture(){
        if (isRunning()) {
            getHandler().post(() -> {
                if (mCapturedStarted) {
                    mVideoCapture.deallocate();
                    mVideoCapture.allocate(mWidth, mHeight, mFrameRate, mFacing);
                    mVideoCapture.startCaptureMaybeAsync(false);
                }
            });
        }
    }

    public void switchCamera() {
        if (isRunning()) {
            getHandler().postAtFrontOfQueue(() -> {
                if (mCapturedStarted) {
                    mVideoCapture.deallocate();
                    switchCameraFacing();
                    mVideoCapture.allocate(mWidth, mHeight, mFrameRate, mFacing);
                    mVideoCapture.startCaptureMaybeAsync(false);
                }
            });
        }
    }

    private void switchCameraFacing() {
        if (mFacing == Constant.CAMERA_FACING_FRONT) {
            mFacing = Constant.CAMERA_FACING_BACK;
        } else if (mFacing == Constant.CAMERA_FACING_BACK) {
            mFacing = Constant.CAMERA_FACING_FRONT;
        }
    }

    void stopCapture() {
        if (isRunning()) {
            getHandler().postAtFrontOfQueue(() -> {
                if (mCapturedStarted) {
                    mVideoCapture.deallocate();
                    mCapturedStarted = false;
                }
            });
        }
    }

    public boolean hasCaptureStarted() {
        return mCapturedStarted;
    }

    public boolean isCameraSteady(){
            return mCapturedStarted && mVideoCapture.isCameraSteady();
    }

    void setCameraStateListener(VideoCapture.VideoCaptureStateListener listener) {
        if (isRunning()) {
            getHandler().postAtFrontOfQueue(() -> mVideoCapture.setCaptureStateListener(listener));
        }
    }

    public int getWidth(){
        return mWidth;
    }
    public int getHeight(){
        return mHeight;
    }
}
