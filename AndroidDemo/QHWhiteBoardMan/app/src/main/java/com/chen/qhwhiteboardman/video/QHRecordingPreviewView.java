package com.chen.qhwhiteboardman.video;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.Surface;
import android.view.SurfaceView;

public class QHRecordingPreviewView extends SurfaceView implements SurfaceHolder.Callback {

    public QHRecordingPreviewView(Context context) {
        super(context);
        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Surface surface = holder.getSurface();
        int width = getWidth();
        int height = getHeight();
        if(null != mCallback){
            mCallback.createSurface(surface, width, height);
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        if(null != mCallback){
            mCallback.resetRenderSize(width, height);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (null != mCallback) {
            mCallback.destroySurface();
        }
    }

    private QHRecordingPreviewViewCallback mCallback;
    public void setCallback(QHRecordingPreviewViewCallback callback){
        this.mCallback = callback;
    }
    public interface QHRecordingPreviewViewCallback {
        public void createSurface(Surface surface, int width, int height);
        public void resetRenderSize(int width, int height);
        public void destroySurface();
    }
}
