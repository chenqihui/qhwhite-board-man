package com.chen.qhwhiteboardman;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.SurfaceView;

import com.chen.qhwhiteboardman.capture.camera.CameraVideoManager;
import com.chen.qhwhiteboardman.capture.camera.Constant;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        doCamera();
    }

    private CameraVideoManager cameraVideoM;

    private void doCamera() {
        cameraVideoM = new CameraVideoManager(this, null);

        int w = 720;
        int h = 1080;
        cameraVideoM.setPictureSize(w, h);
        cameraVideoM.setFrameRate(15);
        cameraVideoM.setFacing(Constant.CAMERA_FACING_BACK);

        SurfaceView view = this.findViewById(R.id.surfaceView);
        cameraVideoM.setLocalPreview(view);

        cameraVideoM.startCapture();
    }
}