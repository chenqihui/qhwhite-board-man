package com.chen.qhwhiteboardman.capture.framework.modules.processors;

import com.chen.qhwhiteboardman.capture.framework.modules.channels.VideoChannel;
import com.chen.qhwhiteboardman.capture.camera.VideoCaptureFrame;

public interface IPreprocessor {
    VideoCaptureFrame onPreProcessFrame(VideoCaptureFrame outFrame, VideoChannel.ChannelContext context);

    void initPreprocessor();

    void enablePreProcess(boolean enabled);

    void releasePreprocessor(VideoChannel.ChannelContext context);

    void setOutputSize(int outWidth, int outHeight);

}
