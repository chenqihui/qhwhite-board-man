package com.chen.qhwhiteboardman.capture.framework.modules.producers;

import com.chen.qhwhiteboardman.capture.camera.VideoCaptureFrame;

public interface IVideoProducer {
    void connectChannel(int channelId);
    void pushVideoFrame(VideoCaptureFrame frame);
    void disconnect();
}
