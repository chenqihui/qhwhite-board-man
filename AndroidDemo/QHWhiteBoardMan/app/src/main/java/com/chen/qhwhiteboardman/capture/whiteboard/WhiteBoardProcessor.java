package com.chen.qhwhiteboardman.capture.whiteboard;

import android.opengl.GLES20;

import com.chen.qhwhiteboardman.capture.camera.VideoCaptureFrame;
import com.chen.qhwhiteboardman.capture.framework.modules.channels.VideoChannel;

public class WhiteBoardProcessor {

    private WhiteBoardProgram whiteBoardProgram;

    public void init(VideoChannel.ChannelContext context) {
        whiteBoardProgram = new WhiteBoardProgram(context.getContext());
    }

    public VideoCaptureFrame process(VideoCaptureFrame frame, VideoChannel.ChannelContext channelContext) {

        int w = frame.format.getWidth();
        int h = frame.format.getHeight();

        if (frame.format.getTexFormat() == GLES20.GL_TEXTURE_2D) {
            whiteBoardProgram.update(w, h);
            frame.textureId = whiteBoardProgram.process(frame.textureId);
        }
        return frame;
    }

    public void release(VideoChannel.ChannelContext mContext) {
        if (whiteBoardProgram != null){
            whiteBoardProgram.destroyProgram();
        }
    }
}
