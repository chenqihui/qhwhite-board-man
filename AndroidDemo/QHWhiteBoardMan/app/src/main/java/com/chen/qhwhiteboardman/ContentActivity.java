package com.chen.qhwhiteboardman;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chen.qhwhiteboardman.video.QHRecordingPreviewView;
import com.chen.qhwhiteboardman.video.QHVideoCamera;
import com.chen.qhwhiteboardman.video.QHRecordingPreviewScheduler;

public class ContentActivity extends AppCompatActivity {

    private RelativeLayout contentLayout;

    private QHRecordingPreviewView previewV;
    private QHVideoCamera videoCamera;
    private QHRecordingPreviewScheduler previewScheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        setup();
    }

    private void setup() {
        setupUI();
        setupCamera();
        setupPreviewScheduler();
    }

    private void setupUI() {
        contentLayout = findViewById(R.id.ContentLayout);
        previewV = new QHRecordingPreviewView(this);
        contentLayout.addView(previewV, 0);
        contentLayout.getLayoutParams().width = getWindowManager().getDefaultDisplay().getWidth();
        contentLayout.getLayoutParams().height = getWindowManager().getDefaultDisplay().getHeight();
    }

    private void setupCamera() {
        videoCamera = new QHVideoCamera(this);
    }

    private void setupPreviewScheduler() {
        previewScheduler = new QHRecordingPreviewScheduler(previewV, videoCamera, this) {
            @Override
            public void onPermissionDismiss(String tip) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ContentActivity.this, tip, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
        }
    };
}