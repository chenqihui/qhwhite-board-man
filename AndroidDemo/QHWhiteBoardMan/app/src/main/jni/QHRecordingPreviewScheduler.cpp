//
// Created by qihuichen on 2023/1/13.
//

#include "com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler.h"
#include <sys/types.h>
#include <android/native_window.h>
#include <android/native_window_jni.h>
#include <android/asset_manager_jni.h>
#include <android/asset_manager.h>
#include "./camera_preview/mv_recording_preview_controller.h"

#include <android/log.h>
#include <android/bitmap.h>
#define LOG_TAG "hpc -- JNILOG"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

static MVRecordingPreviewController *previewController = 0;

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_switchCameraFacing
  (JNIEnv * env, jobject obj) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_switchCameraFacing");
	if (NULL != previewController) {
		previewController->switchCameraFacing();
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_createWindowSurface
  (JNIEnv * env, jobject obj, jobject surface) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_createWindowSurface");
	if (surface != 0 && NULL != previewController) {
		ANativeWindow* window = ANativeWindow_fromSurface(env, surface);
		if (window != NULL) {
			previewController->createWindowSurface(window);
		}
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_destroyWindowSurface
  (JNIEnv * env, jobject obj) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_destroyWindowSurface");
	if (NULL != previewController) {
		previewController->destroyWindowSurface();
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_prepareEGLContext
  (JNIEnv * env, jobject obj, jobject surface, jint screenWidth, jint screenHeight, jint cameraFacingId) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_prepareEGLContext");
	previewController = new MVRecordingPreviewController();
	JavaVM *g_jvm = NULL;
	env->GetJavaVM(&g_jvm);
	jobject g_obj = env->NewGlobalRef(obj);
	if (surface != 0 && NULL != previewController) {
		ANativeWindow* window = ANativeWindow_fromSurface(env, surface);
		if (window != NULL) {
			previewController->prepareEGLContext(window, g_jvm, g_obj, screenWidth, screenHeight, cameraFacingId);
		}
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_destroyEGLContext
  (JNIEnv * env, jobject obj) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_destroyEGLContext");
	if (NULL != previewController) {
		previewController->destroyEGLContext();
		delete previewController;
		previewController = NULL;
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_resetRenderSize
  (JNIEnv * env, jobject obj, jint screenWidth, jint screenHeight) {
    LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_resetRenderSize");
	if (NULL != previewController) {
		previewController->resetRenderSize(screenWidth, screenHeight);
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_notifyFrameAvailable
  (JNIEnv * env, jobject obj) {
    //LOGI("JNICALL_Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_notifyFrameAvailable");
	if (NULL != previewController) {
		previewController->notifyFrameAvailable();
	}
}

JNIEXPORT void JNICALL Java_com_chen_qhwhiteboardman_video_QHRecordingPreviewScheduler_setImgData
  (JNIEnv * env, jobject obj, jintArray buf, jint w, jint h) {
    if (NULL != previewController) {
        jint *cbuf = env->GetIntArrayElements(buf, JNI_FALSE);
    	previewController->setImgData((unsigned char *)cbuf, w, h);
    }
}
