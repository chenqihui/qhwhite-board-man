#include "gpu_texture_frame_copier.h"

#define LOG_TAG "GPUTextureFrameCopier"

GPUTextureFrameCopier::GPUTextureFrameCopier() {
	mVertexShader = NO_FILTER_VERTEX_SHADER_2;
	mFragmentShader = GPU_FRAME_FRAGMENT_SHADER;
}

GPUTextureFrameCopier::~GPUTextureFrameCopier() {
}

bool GPUTextureFrameCopier::init() {
	mGLProgId = loadProgram(mVertexShader, mFragmentShader);
	if (!mGLProgId) {
		LOGE("Could not create program.");
		return false;
	}
	mGLVertexCoords = glGetAttribLocation(mGLProgId, "vPosition");
	checkGlError("glGetAttribLocation vPosition");
	mGLTextureCoords = glGetAttribLocation(mGLProgId, "vTexCords");
	checkGlError("glGetAttribLocation vTexCords");
	mGLTextureCoords2 = glGetAttribLocation(mGLProgId, "uTexCords");
	checkGlError("glGetAttribLocation vTexCords2");
	mGLUniformTexture = glGetUniformLocation(mGLProgId, "yuvTexSampler");
	checkGlError("glGetAttribLocation yuvTexSampler");
	uTextureUnitLocation = glGetUniformLocation(mGLProgId, "uTextureUnit");
	checkGlError("glGetAttribLocation uTextureUnit");

	mUniformTexMatrix = glGetUniformLocation(mGLProgId, "texMatrix");
	checkGlError("glGetUniformLocation mUniformTexMatrix");

	mUniformTransforms = glGetUniformLocation(mGLProgId, "trans");
	checkGlError("glGetUniformLocation mUniformTransforms");

	mIsInitialized = true;
	return true;
}

void GPUTextureFrameCopier::renderWithCoords(TextureFrame* textureFrame, GLuint texId, GLfloat* vertexCoords, GLfloat* textureCoords, GLuint imgTexId, GLfloat* textureCoords2) {
	glBindTexture(GL_TEXTURE_2D, texId);
	checkGlError("glBindTexture");

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			texId, 0);
	checkGlError("glFramebufferTexture2D");

	glUseProgram(mGLProgId);
	if (!mIsInitialized) {
		return;
	}

	if (imgTexId > 0) {
	    glActiveTexture(GL_TEXTURE2);
	    glBindTexture(GL_TEXTURE_2D, imgTexId);
	    glUniform1i(uTextureUnitLocation, 2);
	    checkGlError("imgTexId");
	}

	glVertexAttribPointer(mGLVertexCoords, 2, GL_FLOAT, GL_FALSE, 0, vertexCoords);
	glEnableVertexAttribArray (mGLVertexCoords);
	glVertexAttribPointer(mGLTextureCoords, 2, GL_FLOAT, GL_FALSE, 0, textureCoords);
	glEnableVertexAttribArray (mGLTextureCoords);
	glVertexAttribPointer(mGLTextureCoords2, 2, GL_FLOAT, GL_FALSE, 0, textureCoords2);
	glEnableVertexAttribArray (mGLTextureCoords2);
	/* Binding the input texture */
	textureFrame->bindTexture(&mGLUniformTexture);

	float texTransMatrix[4 * 4];
	matrixSetIdentityM(texTransMatrix);
	glUniformMatrix4fv(mUniformTexMatrix, 1, GL_FALSE, (GLfloat *) texTransMatrix);

	float rotateMatrix[4 * 4];
	matrixSetIdentityM(rotateMatrix);
	glUniformMatrix4fv(mUniformTransforms, 1, GL_FALSE, (GLfloat *) rotateMatrix);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisableVertexAttribArray(mGLVertexCoords);
    glDisableVertexAttribArray(mGLTextureCoords);
    glDisableVertexAttribArray(mGLTextureCoords2);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
//    LOGI("draw waste time is %ld", (getCurrentTime() - startDrawTimeMills));
}
