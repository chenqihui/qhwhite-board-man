#ifndef GPU_TEXTURE_FRAME_COPIER_H
#define GPU_TEXTURE_FRAME_COPIER_H

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "./../../../libcommon/CommonTools.h"
#include "texture_frame_copier.h"
#include "./../../matrix.h"


static char* GPU_FRAME_VERTEX_SHADER =
		"attribute vec4 vPosition;\n"
		"attribute vec4 vTexCords;\n"
		"varying vec2 yuvTexCoords;\n"
		"uniform highp mat4 trans; \n"
		"void main() {\n"
		"  yuvTexCoords = vTexCords.xy;\n"
		"  gl_Position = trans * vPosition;\n"
		"}\n";

static char* NO_FILTER_VERTEX_SHADER_2 =
		"attribute vec4 vPosition;\n"
		"attribute vec4 vTexCords;\n"
		"varying vec2 yuvTexCoords;\n"
		"attribute vec4 uTexCords;\n"
		"varying vec2 uTexCoords;\n"
		"uniform highp mat4 texMatrix;\n"
		"uniform highp mat4 trans; \n"
		"void main() {\n"
		"  yuvTexCoords = (texMatrix*vTexCords).xy;\n"
		"  uTexCoords = uTexCords.xy;\n"
		"  gl_Position = trans * vPosition;\n"
		"}\n";

static char* GPU_FRAME_FRAGMENT_SHADER =
	"#extension GL_OES_EGL_image_external : require\n"
	"precision mediump float;\n"
	"uniform samplerExternalOES yuvTexSampler;\n"
	"uniform sampler2D uTextureUnit;\n"
	"varying vec2 yuvTexCoords;\n"
	"varying vec2 uTexCoords;\n"
	"void main() {\n"
	"  if (uTexCoords.y < 0.5) {\n"
    "      gl_FragColor = vec4(texture2D(uTextureUnit, uTexCoords).bgr, 1);\n"
    "  }\n"
    "  else {\n"
    "      gl_FragColor = texture2D(yuvTexSampler, yuvTexCoords);\n"
    "  }\n"
	"}\n";

class GPUTextureFrameCopier : public TextureFrameCopier{
public:
	GPUTextureFrameCopier();
    virtual ~GPUTextureFrameCopier();

    virtual bool init();
    virtual void renderWithCoords(TextureFrame* textureFrame, GLuint texId, GLfloat* vertexCoords, GLfloat* textureCoords, GLuint imgTexId, GLfloat* textureCoords2);
protected:
    GLint mGLUniformTexture;
    GLint uTextureUnitLocation;
	GLuint mGLTextureCoords2;
};

#endif // GPU_TEXTURE_FRAME_COPIER_H
